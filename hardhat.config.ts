// hardhat.config.ts
import { task } from 'hardhat/config';
import { HardhatUserConfig } from "hardhat/types";

import "@nomiclabs/hardhat-waffle";
import "@nomiclabs/hardhat-etherscan";
import "@symblox/hardhat-dotenv";
import "@typechain/hardhat";

import cfg from './.config';

task("accounts", "Prints the list of accounts", async (_, hre) => {
    const accounts = await hre.ethers.getSigners();

    for (const account of accounts) {
      console.log(account.address);
    }
});

const config: HardhatUserConfig = {
    defaultNetwork: "hardhat",
    networks: {
        localhost: {
            url: "http://127.0.0.1:8545"
        },
        hardhat: {
            // loggingEnabled: true,
        },
        matic: {
            url: "https://rpc-mainnet.maticvigil.com/",
            chainId: 137,
            gasPrice: 1000000000,
            accounts: cfg.accounts,
        },
        maticTest: {
            url: 'https://rpc-mumbai.maticvigil.com/',
            chainId: 80001,
            gasPrice: 1000000000,
            accounts: cfg.accounts,
        },
        bsc: {
            url: "https://bsc-dataseed.binance.org/",
            chainId: 56,
            gasPrice: 5000000000,
            accounts: cfg.accounts,
        },
        bscTest: {
            url: "https://data-seed-prebsc-1-s1.binance.org:8545",
            chainId: 97,
            gasPrice: 5000000000,
            accounts: cfg.accounts,
        },
    },
    etherscan: {
        apiKey: cfg.bscApiKey,
    },
    solidity: {
        version: '0.8.0',
        compilers: [{version: '0.8.0'}],
        settings: {
            optimizer: {
                enabled: true,
                runs: 200
            }
        }
    },
};
export default config;
