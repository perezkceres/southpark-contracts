const hre = require("hardhat");

async function deployToken(operator, router) {
    const SouthParkToken = await hre.ethers.getContractFactory("SouthParkToken");
    const token = await SouthParkToken.deploy(operator, router);
    await token.deployTransaction.wait();
    console.log("SouthParkToken: ", token.address);
    return token;
}

async function deployMasterChef(kenny, dev, fee, startBlock, rAllocPoint) {
    const MasterChef = await hre.ethers.getContractFactory("MasterChef");
    const masterchef = await MasterChef.deploy(kenny, dev, fee, startBlock, rAllocPoint);
    await masterchef.deployTransaction.wait()
    console.log("MasterChef: ", masterchef.address);
    return masterchef;
}

async function deployTimelock(owner, delay) {
    const Timelock = await hre.ethers.getContractFactory("Timelock");
    const timelock = await Timelock.deploy(owner, delay);
    await timelock.deployTransaction.wait()
    console.log(`Timelock: ${timelock.address}`);
    return timelock;
}

async function main() {
    const owner = '0x7a7AB933DFe72efFb23C66D9fa6f9783F9FDb540';
    const operator = '0xE96AC6F39e200f70a8B6BD3f50467AA7b54c5039';
    const dev = '0x507673158f8cd0A415D6b674BDA2A964E9316632';
    const fee = '0x106b250d279dce8D7DB73bABB507C60A0Db64CD5';

    const router = '0x05fF2B0DB69458A0750badebc4f9e13aDd608C7F';

    const kennyAddr = '';

    if (!kennyAddr) {
        await deployToken(operator, router);
    } else {
        await deployTimelock(owner, 24 * 60 * 60);
        await deployMasterChef(kennyAddr, dev, fee, 0, 1000);
    }
}

main()
  .then(() => process.exit(0))
  .catch(error => {
    console.error(error);
    process.exit(1);
  });
