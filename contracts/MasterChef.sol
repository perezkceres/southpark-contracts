// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";

import "./SouthParkToken.sol";

contract MasterChef is Ownable, IHaveReinvestPool {
    using SafeERC20 for ERC20;

    // Info of each user.
    struct UserInfo {
        uint amount;         // How many LP tokens the user has provided.
        uint rewardDebt;     // Reward debt. See explanation below.
        uint rewardLocked;   // Locked reward.
        uint lastClaimTime;  // Timestamp of last claim or reinvest.
    }

    // Info of each pool.
    struct PoolInfo {
        uint totalStake;
        ERC20 lpToken;         // Address of LP token contract.
        uint allocPoint;       // How many allocation points assigned to this pool. KENNY to distribute per block.
        uint lastRewardBlock;  // Last block number that KENNY distribution occurs.
        uint accKennyPerStake; // Accumulated KENNY per share, times 1e12. See below.
        uint16 depositFeeBP;   // Deposit fee in basis points
    }

    SouthParkToken public kenny;
    // KENNY tokens created per block.
    uint public constant kennyPerBlock = 50 ether;

    // Dev address.
    address public devAddress;
    // Deposit Fee address
    address public feeAddress;

    // Info of each pool.
    PoolInfo[] public poolInfo;

    // Added tokens;
    mapping (ERC20 => bool) tokens;

    // Info of each user that stakes LP tokens.
    mapping (uint => mapping (address => UserInfo)) public userInfo;

    // Referrer info
    mapping (address => address) public referrer;
    mapping (address => mapping (uint8 => uint)) public referrals;
    mapping (address => uint) public referrerReward;

    // Total allocation points. Must be the sum of all allocation points in all pools.
    uint public totalAllocPoint = 0;
    // The block number when Kenny mining starts.
    uint public startBlock;
    // Harvest/Reinvest lockup time
    uint16 public constant harvestLockupTime = 4 hours;
    // Referrers reward rate: 1%, 0.3%, 0.1%
    uint8[] public referrerRewardRate = [ 100, 30, 10 ];
    // Total reinvest pool reward from KENNY token.
    uint public totalReinvestReward = 0;

    event Deposit(address indexed user, uint indexed pid, uint amount);
    event Withdraw(address indexed user, uint indexed pid, uint amount);
    event Reinvest(address indexed user, uint indexed pid, uint amount);
    event EmergencyWithdraw(address indexed user, uint indexed pid, uint amount);

    constructor(
        SouthParkToken _kenny,
        address _devAddress,
        address _feeAddress,
        uint _startBlock,
        uint reinvestAllocPoint
    ) {
        require(_devAddress != address(0), "address can't be 0");
        require(_feeAddress != address(0), "address can't be 0");

        kenny = _kenny;
        devAddress = _devAddress;
        feeAddress = _feeAddress;
        startBlock = _startBlock;

        // add reinvest pool
        poolInfo.push(PoolInfo({
            totalStake: 0,
            lpToken: kenny,
            allocPoint: reinvestAllocPoint,
            lastRewardBlock: startBlock,
            accKennyPerStake: 0,
            depositFeeBP: 0
        }));
        totalAllocPoint += reinvestAllocPoint;
    }

    function poolLength() external view returns (uint) {
        return poolInfo.length;
    }

    // Add a new lp to the pool. Can only be called by the owner.
    function add(ERC20 lpToken, uint allocPoint, uint16 depositFeeBP, bool withUpdate) public onlyOwner {
        require(!tokens[lpToken], "add: token already added");
        require(depositFeeBP <= 10000, "add: invalid deposit fee basis points");

        if (withUpdate) {
            massUpdatePools();
        }

        uint lastRewardBlock = block.number > startBlock ? block.number : startBlock;
        totalAllocPoint += allocPoint;
        poolInfo.push(PoolInfo({
            totalStake:0,
            lpToken: lpToken,
            allocPoint: allocPoint,
            lastRewardBlock: lastRewardBlock,
            accKennyPerStake: 0,
            depositFeeBP: depositFeeBP
        }));
        tokens[lpToken] = true;
    }

    // Update the given pool's KENNY allocation point and deposit fee. Can only be called by the owner.
    function set(uint pid, uint allocPoint, uint16 depositFeeBP, bool withUpdate) public onlyOwner {
        require(depositFeeBP <= 10000, "set: invalid deposit fee basis points");

        if (withUpdate) {
            massUpdatePools();
        }

        totalAllocPoint = totalAllocPoint - poolInfo[pid].allocPoint + allocPoint;
        poolInfo[pid].allocPoint = allocPoint;
        poolInfo[pid].depositFeeBP = depositFeeBP;
    }

    // View function to see pending KENNY on frontend.
    function pendingKenny(uint pid, address user) external view returns (uint) {
        return _pendingKenny(pid, user);
    }

    // Deposit LP tokens to MasterChef for KENNY allocation.
    function deposit(uint pid, uint amount, address ref) external {
        require (pid != 0, "reinvest pool disallow deposit");

        PoolInfo storage pool = poolInfo[pid];
        UserInfo storage user = userInfo[pid][msg.sender];

        updatePool(pid);

        if (canClaim(pid)) {
            claim(pid);
        } else {
            lockup(pid);
        }

        uint oldAmount = user.amount;
        if (amount > 0) {
            pool.lpToken.safeTransferFrom(address(msg.sender), address(this), amount);

            if (pool.depositFeeBP > 0){
                uint depositFee = amount * pool.depositFeeBP / 10000;
                pool.lpToken.safeTransfer(feeAddress, depositFee);
                user.amount += amount - depositFee;
            } else {
                user.amount += amount;
            }
        }

        pool.totalStake = pool.totalStake - oldAmount + user.amount;
        user.rewardDebt = user.amount * pool.accKennyPerStake / 1e12;

        if (ref != address(0) && ref != msg.sender && referrer[msg.sender] == address(0)) {
            referrer[msg.sender] = ref;

            // level -1
            referrals[ref][0] += 1;
            referrals[ref][1] += referrals[msg.sender][0];
            referrals[ref][2] += referrals[msg.sender][1];

            // level -2
            address ref1 = referrer[ref];
            if (ref1 != address(0)) {
                referrals[ref1][1] += 1;
                referrals[ref1][2] += referrals[msg.sender][0];

                // level -3
                address ref2 = referrer[ref1];
                if (ref2 != address(0)) {
                    referrals[ref2][2] += 1;
                }
            }
        }

        emit Deposit(msg.sender, pid, amount);
    }

    // Withdraw LP tokens from MasterChef.
    function withdraw(uint pid, uint amount) external {
        PoolInfo storage pool = poolInfo[pid];
        UserInfo storage user = userInfo[pid][msg.sender];
        require(user.amount >= amount, "withdraw: not good");

        updatePool(pid);

        if (canClaim(pid)) {
            claim(pid);
        } else {
            lockup(pid);
        }

        if (amount > 0) {
            user.amount -= amount;
            pool.lpToken.safeTransfer(address(msg.sender), amount);
        }

        pool.totalStake -= amount;
        user.rewardDebt = user.amount * pool.accKennyPerStake / 1e12;
        emit Withdraw(msg.sender, pid, amount);
    }

    // Reinvest LP tokens to Reinvest Pool
    function reinvest(uint pid) public {
        require (canClaim(pid), "reinvest locked");

        PoolInfo storage pool = poolInfo[pid];
        UserInfo storage user = userInfo[pid][msg.sender];
        PoolInfo storage rPool = poolInfo[0];
        UserInfo storage rUser = userInfo[0][msg.sender];

        updatePool(pid);

        uint toReinvest = _pendingKenny(pid, msg.sender);
        if (toReinvest == 0) {
            return;
        }

        if (pid != 0) {
            updatePool(0);
            lockup(0);
        }

        rUser.amount += toReinvest;
        rPool.totalStake += toReinvest;
        rUser.rewardDebt = rUser.amount * rPool.accKennyPerStake / 1e12;

        user.rewardLocked = 0;
        user.rewardDebt = user.amount * pool.accKennyPerStake / 1e12;
        user.lastClaimTime = block.timestamp;

        if (pid != 0) {
            rewardReferrers(toReinvest);
        }

        emit Reinvest(msg.sender, pid, toReinvest);
    }

    // Withdraw without caring about rewards. EMERGENCY ONLY.
    function emergencyWithdraw(uint pid) external {
        PoolInfo storage pool = poolInfo[pid];
        UserInfo storage user = userInfo[pid][msg.sender];

        pool.lpToken.safeTransfer(address(msg.sender), user.amount);
        emit EmergencyWithdraw(msg.sender, pid, user.amount);

        pool.totalStake -= user.amount;
        user.amount = 0;
        user.rewardDebt = 0;
        user.rewardLocked = 0;
    }

    function addReinvestReward(uint amount) external override {
        require(msg.sender == address(kenny), "KENNY only allowed");

        PoolInfo storage pool = poolInfo[0];

        if (pool.totalStake > 0) {
            totalReinvestReward += amount;
            pool.accKennyPerStake += amount * 1e12 / pool.totalStake;
        }
    }

    // Update reward variables of the given pool to be up-to-date.
    function updatePool(uint pid) internal {
        PoolInfo storage pool = poolInfo[pid];
        if (block.number <= pool.lastRewardBlock) {
            return;
        }
        if (pool.totalStake == 0 || pool.allocPoint == 0) {
            pool.lastRewardBlock = block.number;
            return;
        }

        uint blockAmount = block.number - pool.lastRewardBlock;
        uint kennyReward = blockAmount * kennyPerBlock * pool.allocPoint / totalAllocPoint;

        kenny.mint(devAddress, kennyReward / 10);
        kenny.mint(address(this), kennyReward);

        pool.accKennyPerStake += kennyReward * 1e12 / pool.totalStake;
        pool.lastRewardBlock = block.number;
    }

    // Update reward variables for all pools. Be careful of gas spending!
    function massUpdatePools() internal {
        uint length = poolInfo.length;
        for (uint pid = 0; pid < length; ++pid) {
            updatePool(pid);
        }
    }

    function lockup(uint pid) internal {
        userInfo[pid][msg.sender].rewardLocked = _pendingKenny(pid, msg.sender);
    }

    function rewardReferrers(uint baseAmount) internal {
        address ref = msg.sender;
        for (uint8 i = 0; i < referrerRewardRate.length; i++) {
            ref = referrer[ref];
            if (ref == address(0)) {
                break;
            }

            uint reward = baseAmount * referrerRewardRate[i] / 10000;
            kenny.mint(ref, reward);
            referrerReward[ref] += reward;
        }
    }

    function claim(uint pid) internal {
        uint pending = _pendingKenny(pid, msg.sender);
        if (pending > 0) {
            safeKennyTransfer(msg.sender, pending);
            if (pid != 0) {
                rewardReferrers(pending);
            }
        }

        userInfo[pid][msg.sender].rewardLocked = 0;
        userInfo[pid][msg.sender].lastClaimTime = block.timestamp;
    }

    function canClaim(uint pid) internal view returns (bool) {
        return userInfo[pid][msg.sender].lastClaimTime + harvestLockupTime < block.timestamp;
    }

    function _pendingKenny(uint pid, address _user) internal view returns (uint) {
        PoolInfo storage pool = poolInfo[pid];
        UserInfo storage user = userInfo[pid][_user];

        if (user.amount == 0) {
            return user.rewardLocked;
        }

        uint accKennyPerStake = pool.accKennyPerStake;
        if (block.number > pool.lastRewardBlock && pool.totalStake > 0) {
            uint blockAmount = block.number - pool.lastRewardBlock;
            uint kennyReward = blockAmount * kennyPerBlock * pool.allocPoint / totalAllocPoint;
            accKennyPerStake += kennyReward * 1e12 / pool.totalStake;
        }

        return user.rewardLocked + user.amount * accKennyPerStake / 1e12 - user.rewardDebt;
    }

    // Safe kenny transfer function, just in case if rounding error causes pool to not have enough KENNY.
    function safeKennyTransfer(address to, uint amount) internal {
        uint kennyBal = kenny.balanceOf(address(this));
        if (amount > kennyBal) {
            kenny.transfer(to, kennyBal);
        } else {
            kenny.transfer(to, amount);
        }
    }

    // Update dev address by the previous dev.
    function setDevAddress(address newDevAddress) public {
        require(msg.sender == devAddress, "setDevAddress: FORBIDDEN");
        devAddress = newDevAddress;
    }

    function setFeeAddress(address newFeeAddress) public {
        require(msg.sender == feeAddress, "setFeeAddress: FORBIDDEN");
        feeAddress = newFeeAddress;
    }
}
