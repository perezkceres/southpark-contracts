// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import "@openzeppelin/contracts/utils/Address.sol";
import "@uniswap/v2-periphery/contracts/interfaces/IUniswapV2Router02.sol";

import "./OwnableToken.sol";

interface IHaveReinvestPool {
  function addReinvestReward(uint amount) external;
}

contract SouthParkToken is OwnableToken {
  // max 10%
  uint16 public constant MAX_TRANSFER_TAX_RATE = 1000;

  // transfer tax 5%
  uint16 public transferTaxRate = 500;
  // 40% of transfer tax sends into Reinvest Pool
  uint16 public constant taxRewardPartRate = 40;

  bool public autoLiqEnabled = false;
  uint public autoLiqMinAmount = 500 ether;

  IUniswapV2Router02 router;
  address kennyEthPair;

  // The operator can only update transfer tax and autoLiq params
  address public operator;

  event TaxRateChanged(uint16 newTax);
  event AutoLiq(uint tokenAmount, uint ethAmount);

  modifier onlyOperator() {
    require(operator == msg.sender, "operator: caller is not the operator");
    _;
  }

  constructor(
    address _operator,
    IUniswapV2Router02 _router
  ) OwnableToken("South Park Token", "KENNY") {
    operator = _operator;
    router = _router;
  }

  function setKennyEthPair(address pair) external onlyOwner {
    kennyEthPair = pair;
  }

  function setTransferTaxRate(uint16 newRate) external onlyOperator {
    require(newRate <= MAX_TRANSFER_TAX_RATE, "KENNY: max transfer tax rate exceed");
    transferTaxRate = newRate;
    emit TaxRateChanged(newRate);
  }

  function setAutoLiqMinAmount(uint minAmount) external onlyOperator {
    require(minAmount >= 10 ether, "minimum is 10 ether");
    autoLiqMinAmount = minAmount;
  }

  function setAutoLiqEnabled(bool enabled) external onlyOperator {
    autoLiqEnabled = enabled;
  }

  function _transfer(address sender, address recipient, uint256 amount) internal virtual override {
    if (autoLiqEnabled && sender != owner() && sender != kennyEthPair && sender != address(router)) {
      autoLiq();
    }

    if (transferTaxRate > 0 && kennyEthPair != address(0)) {
      uint taxAmount = amount * transferTaxRate / 10000;
      uint rewardAmount = taxAmount * taxRewardPartRate / 100;
      uint autoLiqAmount = taxAmount - rewardAmount;

      // transfer for auto liq
      super._transfer(sender, address(this), autoLiqAmount);

      // transfer reward to masterchef
      _approve(address(this), owner(), rewardAmount);
      super._transfer(sender, owner(), rewardAmount);
      IHaveReinvestPool(owner()).addReinvestReward(rewardAmount);

      amount -= taxAmount;
    }

    super._transfer(sender, recipient, amount);
  }

  function autoLiq() private {
    uint thisBalance = balanceOf(address(this));

    if (thisBalance >= autoLiqMinAmount) {
      autoLiqEnabled = false;

      uint liqAmount = autoLiqMinAmount;

      // split amount
      uint swapAmount = liqAmount / 2;
      uint tokenAmount = liqAmount - swapAmount;

      // save current eth balance
      uint ethBalance = address(this).balance;
      // make swap
      swapTokensForEth(swapAmount);
      uint ethAmount = address(this).balance - ethBalance;

      addLiquidity(tokenAmount, ethAmount);
      emit AutoLiq(tokenAmount, ethAmount);

      autoLiqEnabled = true;
    }
  }

  function swapTokensForEth(uint amount) private {
    address[] memory route = new address[](2);
    route[0] = address(this);
    route[1] = router.WETH();

    _approve(address(this), address(router), amount);

    // accept any amount of ETH
    router.swapExactTokensForETHSupportingFeeOnTransferTokens(
      amount, 0, route, address(this), block.timestamp
    );
  }

  function addLiquidity(uint tokenAmount, uint ethAmount) private {
    _approve(address(this), address(router), tokenAmount);

    // slippage is unavoidable
    router.addLiquidityETH{value: ethAmount}(
      address(this), tokenAmount, 0, 0, operator, block.timestamp
    );
  }

  receive() external payable {}
}
