import { SignerWithAddress } from '@nomiclabs/hardhat-ethers/signers';
import { BigNumber } from '@ethersproject/bignumber';
import { solidity } from "ethereum-waffle";
import chai from "chai";

import { ethers } from 'hardhat';
import '@nomiclabs/hardhat-ethers'

import { SouthParkToken, MasterChef } from "../typechain";

chai.use(solidity);
const { expect } = chai;
const decimals = BigNumber.from(10).pow(18);

describe("MasterChef", () => {
  let kenny: SouthParkToken;
  let masterchef: MasterChef;
  let owner: SignerWithAddress;
  let dev: SignerWithAddress;
  let fee: SignerWithAddress;
  let user: SignerWithAddress;

  beforeEach(async () => {
    [owner, dev, fee, user] = await ethers.getSigners()

    const sptFactory = await ethers.getContractFactory("SouthParkToken", owner);
    const mcFactory = await ethers.getContractFactory("MasterChef", owner);

    kenny = (await sptFactory.deploy(owner.address, owner.address)) as SouthParkToken;
    masterchef = (await mcFactory.deploy(kenny.address, dev.address, fee.address, 0, 1000)) as MasterChef;
    await masterchef.add(kenny.address, 1000, 0, false);
    await kenny.mint(user.address, 100);
    await kenny.transferOwnership(masterchef.address);
  });

  describe("referral", async () => {
    it("should work correct", async () => {
      const users = (await ethers.getSigners()).slice(4)

      await masterchef.connect(users[1]).deposit(1, 0, users[0].address)
      await masterchef.connect(users[2]).deposit(1, 0, users[0].address)
      await masterchef.connect(users[3]).deposit(1, 0, users[1].address)
      await masterchef.connect(users[4]).deposit(1, 0, users[1].address)
      await masterchef.connect(users[5]).deposit(1, 0, users[2].address)

      await masterchef.connect(users[7]).deposit(1, 0, users[6].address)
      await masterchef.connect(users[8]).deposit(1, 0, users[6].address)
      await masterchef.connect(users[9]).deposit(1, 0, users[7].address)
      await masterchef.connect(users[10]).deposit(1, 0, users[7].address)
      await masterchef.connect(users[11]).deposit(1, 0, users[8].address)

      expect(await masterchef.referrals(users[0].address, 0)).eq(2)
      expect(await masterchef.referrals(users[0].address, 1)).eq(3)
      expect(await masterchef.referrals(users[0].address, 2)).eq(0)
      expect(await masterchef.referrals(users[1].address, 0)).eq(2)
      expect(await masterchef.referrals(users[1].address, 1)).eq(0)
      expect(await masterchef.referrals(users[2].address, 0)).eq(1)

      await masterchef.connect(users[6]).deposit(1, 0, users[4].address)

      expect(await masterchef.referrals(users[0].address, 2)).eq(1)
      expect(await masterchef.referrals(users[1].address, 1)).eq(1)
      expect(await masterchef.referrals(users[4].address, 0)).eq(1)
      expect(await masterchef.referrals(users[4].address, 1)).eq(2)
      expect(await masterchef.referrals(users[4].address, 2)).eq(3)

      await kenny.connect(user).approve(masterchef.address, ethers.constants.MaxUint256)
      await masterchef.connect(user).deposit(1, 100, users[11].address)
      await ethers.provider.send('evm_increaseTime', [4 * 60 * 60])
      await ethers.provider.send('evm_mine', [])
      await masterchef.connect(user).withdraw(1, 0)

      const reward = decimals.mul(50)
      expect(await kenny.balanceOf(user.address)).eq(reward)
      expect(await kenny.balanceOf(users[11].address)).eq(reward.mul(100).div(10000))
      expect(await kenny.balanceOf(users[8].address)).eq(reward.mul(30).div(10000))
      expect(await kenny.balanceOf(users[6].address)).eq(reward.mul(10).div(10000))

      await ethers.provider.send('evm_increaseTime', [4 * 60 * 60])
      await ethers.provider.send('evm_mine', [])
      await masterchef.connect(user).reinvest(1)

      const reinvestReward = decimals.mul(50)
      expect(await kenny.balanceOf(user.address)).eq(reward)
      expect(await kenny.balanceOf(users[11].address)).eq(reward.add(reinvestReward).mul(100).div(10000))
      expect(await kenny.balanceOf(users[8].address)).eq(reward.add(reinvestReward).mul(30).div(10000))
      expect(await kenny.balanceOf(users[6].address)).eq(reward.add(reinvestReward).mul(10).div(10000))
    });
  });
});
